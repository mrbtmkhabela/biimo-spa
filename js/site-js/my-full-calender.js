let calendar = null;

(() => {
    initCalender();
})();

function initCalender() {
    getUserType = JSON.parse(cookieFunc.get('currentUser'));
    if (getUserType.role === "admin" && getUserType.password === "admin") {
        adminFullCalender();
    }
    else if (getUserType.role === "patron" && getUserType.password === "patron") {
        patronCalender();
    }
    else {
        guestCalender();
    }
}

function adminFullCalender() {
    if (!cookieFunc.get('promotions')) {
        cookieFunc.set('promotions', []);
    }
    const
        Calendar = FullCalendar.Calendar,
        calendarEl = document.getElementById('main-index-calendar'),
        nowDate = new Date(),
        today = new Date(Date.UTC(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()));


    calendar = new Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list', 'bootstrap'],
        defaultView: 'dayGridMonth',
        timeZone: 'UTC',
        themeSystem: 'bootstrap',
        navLinks: true,
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'addEventButton,dayGridMonth,timeGridWeek,timeGridDay'
        },
        customButtons: {
            addEventButton: {
                text: 'add promotion',
                click: function () {
                    createSpecialsForDaySpa();
                }
            }
        },
        eventClick: (info) => {
            const eventObj = info.event;
            console.log("INFO", eventObj);
            console.log("title", eventObj.title);
        },
        eventSources: [

            {
                events: [
                    {
                        title: 'Full Day Treatments Promotion, 21% off only @ our Sandton Branch',
                        start: today
                    }
                ]
            },
            {
                events: displayEvents('promotions'),
            }

        ]
    });
    calendar.render();
}

function guestCalender() {
    const
        Calendar = FullCalendar.Calendar,
        calendarEl = document.getElementById('main-index-calendar');
    calendar = new Calendar(calendarEl, {
        plugins: ['interaction', 'dayGrid', 'timeGrid', 'list', 'bootstrap'],
        defaultView: 'dayGridMonth',
        timeZone: 'UTC',
        themeSystem: 'bootstrap',
        navLinks: true,
        editable: true,
        droppable: true, // this allows things to be dropped onto the calendar
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'addEventButton,dayGridMonth,timeGridWeek,timeGridDay'
        },
        customButtons: {
            addEventButton: {
                text: 'make a booking',
                click: function () {
                    setAppointment();
                }
            }
        },
        dateClick: (info) => {
            setAppointment(info)
        },
        eventClick: (arg) => {
            // prevents current tab from navigating
            arg.jsEvent.preventDefault();
        },
        eventSources: [
            // your event source
            {
                events: function (fetchInfo, successCallback, failureCallback) {
                    // ...
                },
                color: 'yellow',   // an option!
                textColor: 'black' // an option!
            }
            // any other sources...
        ]
    });
    calendar.render();
}

function patronCalender() {

}

function createSpecialsForDaySpa() {
    initDatePicker();
    $('#createSpecialsModalBtn').click();
}

function setAppointment() {
    console.log("Set Appointment");
}

function addEventOnCalender() {
    calendar.render();
    console.log("THIS IS THE ADD EVENT ON CALENDER METHOD");
}

function savePromotion() {
    const form = $('#createSpecialsFormId').serializeArray();

    updateCookie('promotions', form)
    addEventOnCalender();
    resetForm();
}

function initDatePicker() {
    const nowDate = new Date(),
        today = new Date(Date.UTC(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()));
    $('input[name="promotionDateTimePicker"]').daterangepicker({
        autoUpdateInput: true,
        timePicker: true,
        minDate: today,
        locale: {
            format: 'YYYY/MM/DD HH:mm',
            cancelLabel: 'Clear'
        }
    });
    $('input[name="promotionDateTimePicker"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('YYYY/MM/DD HH:mm') + ' - ' + picker.endDate.format('YYYY/MM/DD HH:mm'));
    });

    $('input[name="promotionDateTimePicker"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
}

function closeAddPromotionModal() {
    //resetForm();
}

function resetForm() {
    console.log("THIS IS THE RESET FOR METHOD")
    $('#closeCreateSpecialsModalBtn').click();
    $('#promotionDateTimePicker').val("");
    $('#locationSelector').prop('selectedIndex', 0);
    $('#promotionSelector').prop('selectedIndex', 0);
    $('#discountPercentRange').val("0");
    $('#descriptionTextArea').val("");
}

function updateCookie(cookieName, cookieValue) {
    console.log("UPDATED COOKIE FUNC VAL : ", cookieValue);
    let calenderEvents = getCalenderEvents(cookieName);

    if (cookieName === 'promotions' && cookieValue) {
        calenderEvents.push({
            duration: cookieValue[0].value,
            location: cookieValue[1].value,
            promotion: cookieValue[2].value,
            discount: cookieValue[3].value,
            description: cookieValue[4].value
        });
        cookieFunc.set(cookieName, calenderEvents);
    }
    else if (cookieName === 'bookings' && currentCalenderEvents) {
    }
}

function getCalenderEvents(cookieName) {
    const promotionsCookie = cookieFunc.get(cookieName);

    if (!promotionsCookie) {
        return null;
    }
    else if (promotionsCookie.length < 1) {
        return promotionsCookie;
    }

    const promotions = JSON.parse(promotionsCookie);

    return promotions.map(p => {
        return {
            duration: p.duration,
            promotion: p.promotion,
            discount: p.discount,
            description: p.description,
            location: p.location
        };
    });
}

function displayEvents(cookieName) {
    return getCalenderEvents(cookieName).map(e => {
        const duration = e.duration.split('-'),
            start = duration[0]
                .replace('/', '-')
                .replace('/', '-')
                .trim()
                .replace(' ', 'T'),
            end = duration[1]
                .replace('/', '-')
                .replace('/', '-')
                .trim()
                .replace(' ', 'T');
        console.log("START : ", start);
        console.log("END : ", end);

        return {
            title: `${e.promotion}, ${e.discount}% off only @ our ${e.location}`,
            start: start,
            end: end
        }
    });
}