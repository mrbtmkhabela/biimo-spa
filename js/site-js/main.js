AOS.init({
	duration: 800,
	easing: 'slide',
	once: false
});

jQuery(document).ready(function ($) {

	"use strict";


	let siteMenuClone = function () {

		$('.js-clone-nav').each(function () {
			let $this = $(this);
			$this.clone().attr('class', 'site-nav-wrap').appendTo('.site-mobile-menu-body');
		});


		setTimeout(function () {

			let counter = 0;
			$('.site-mobile-menu .has-children').each(function () {
				let $this = $(this);

				$this.prepend('<span class="arrow-collapse collapsed">');

				$this.find('.arrow-collapse').attr({
					'data-toggle': 'collapse',
					'data-target': '#collapseItem' + counter,
				});

				$this.find('> ul').attr({
					'class': 'collapse',
					'id': 'collapseItem' + counter,
				});

				counter++;

			});

		}, 1000);

		$('body').on('click', '.arrow-collapse', function (e) {
			let $this = $(this);
			if ($this.closest('li').find('.collapse').hasClass('show')) {
				$this.removeClass('active');
			} else {
				$this.addClass('active');
			}
			e.preventDefault();

		});

		$(window).resize(function () {
			let $this = $(this),
				w = $this.width();

			if (w > 768) {
				if ($('body').hasClass('offcanvas-menu')) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		})

		$('body').on('click', '.js-menu-toggle', function (e) {
			let $this = $(this);
			e.preventDefault();

			if ($('body').hasClass('offcanvas-menu')) {
				$('body').removeClass('offcanvas-menu');
				$this.removeClass('active');
			} else {
				$('body').addClass('offcanvas-menu');
				$this.addClass('active');
			}
		})

		// click outisde offcanvas
		$(document).mouseup(function (e) {
			let container = $(".site-mobile-menu");
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				if ($('body').hasClass('offcanvas-menu')) {
					$('body').removeClass('offcanvas-menu');
				}
			}
		});
	};
	siteMenuClone();


	let sitePlusMinus = function () {
		$('.js-btn-minus').on('click', function (e) {
			e.preventDefault();
			if ($(this).closest('.input-group').find('.form-control').val() != 0) {
				$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) - 1);
			} else {
				$(this).closest('.input-group').find('.form-control').val(parseInt(0));
			}
		});
		$('.js-btn-plus').on('click', function (e) {
			e.preventDefault();
			$(this).closest('.input-group').find('.form-control').val(parseInt($(this).closest('.input-group').find('.form-control').val()) + 1);
		});
	};
	// sitePlusMinus();


	let siteSliderRange = function () {
		$("#slider-range").slider({
			range: true,
			min: 0,
			max: 500,
			values: [75, 300],
			slide: function (event, ui) {
				$("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
			}
		});
		$("#amount").val("$" + $("#slider-range").slider("values", 0) +
			" - $" + $("#slider-range").slider("values", 1));
	};
	// siteSliderRange();

	let siteCarousel = function () {
		if ($('.nonloop-block-13').length > 0) {
			$('.nonloop-block-13').owlCarousel({
				center: false,
				items: 1,
				loop: true,
				stagePadding: 0,
				margin: 20,
				smartSpeed: 1000,
				autoplay: true,
				nav: true,
				responsive: {
					600: {
						margin: 20,
						nav: true,
						items: 2
					},
					1000: {
						margin: 20,
						stagePadding: 0,
						nav: true,
						items: 3
					}
				}
			});
			$('.custom-next').click(function (e) {
				e.preventDefault();
				$('.nonloop-block-13').trigger('next.owl.carousel');
			})
			$('.custom-prev').click(function (e) {
				e.preventDefault();
				$('.nonloop-block-13').trigger('prev.owl.carousel');
			})


		}

		if ($('.nonloop-block-6').length > 0) {
			$('.nonloop-block-6').owlCarousel({
				center: true,
				items: 1,
				loop: true,
				stagePadding: 1,
				margin: 22,
				smartSpeed: 1000,
				autoplay: true,
				nav: false,
				responsive: {
					600: {
						margin: 22,
						stagePadding: 1,
						nav: false,
						items: 2
					},
					1000: {
						margin: 22,
						stagePadding: 1,
						nav: false,
						items: 3
					}
				}
			});
			$('.custom-next').click(function (e) {
				e.preventDefault();
				$('.nonloop-block-6').trigger('next.owl.carousel');
			})
			$('.custom-prev').click(function (e) {
				e.preventDefault();
				$('.nonloop-block-6').trigger('prev.owl.carousel');
			})


		}


		$('.slide-one-item').owlCarousel({
			center: true,
			items: 1,
			loop: true,
			stagePadding: 0,
			margin: 0,
			smartSpeed: 1500,
			autoplay: true,
			pauseOnHover: false,
			dots: false,
			nav: true,
			zoom: true,
			navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
		});

		if ($('.owl-all').length > 0) {
			$('.owl-all').owlCarousel({
				center: false,
				items: 1,
				loop: false,
				stagePadding: 0,
				margin: 0,
				autoplay: false,
				nav: false,
				dots: true,
				touchDrag: true,
				mouseDrag: true,
				smartSpeed: 1000,
				navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
				responsive: {
					768: {
						margin: 30,
						nav: false,
						responsiveRefreshRate: 10,
						items: 1
					},
					992: {
						margin: 30,
						stagePadding: 0,
						nav: false,
						responsiveRefreshRate: 10,
						touchDrag: false,
						mouseDrag: false,
						items: 3
					},
					1200: {
						margin: 30,
						stagePadding: 0,
						nav: false,
						responsiveRefreshRate: 10,
						touchDrag: false,
						mouseDrag: false,
						items: 3
					}
				}
			});
		}

	};
	siteCarousel();

	let siteDatePicker = function () {

		if ($('.datepicker').length > 0) {
			$('.datepicker').datepicker();
		}

	};
	siteDatePicker();

	let siteSticky = function () {
		$(".js-sticky-header").sticky({ topSpacing: 0 });
	};
	siteSticky();

	// navigation
	let OnePageNavigation = function () {
		let navToggler = $('.site-menu-toggle');

		$("body").on("click", ".main-menu li a[href^='#'], .smoothscroll[href^='#'], .site-mobile-menu .site-nav-wrap li a[href^='#']", function (e) {
			e.preventDefault();

			let hash = this.hash;

			$('html, body').animate({
				'scrollTop': $(hash).offset().top - 50
			}, 600, 'easeInOutExpo', function () {
				// window.location.hash = hash;

			});

		});
	};
	OnePageNavigation();

	let siteScroll = function () {



		$(window).scroll(function () {

			let st = $(this).scrollTop();

			if (st > 100) {
				$('.js-sticky-header').addClass('shrink');
			} else {
				$('.js-sticky-header').removeClass('shrink');
			}

		})

	};
	siteScroll();

	// Stellar
	$(window).stellar({
		horizontalScrolling: false,
		responsive: true,
	});


	let counter = function () {

		$('#about-section').waypoint(function (direction) {

			if (direction === 'down' && !$(this.element).hasClass('ftco-animated')) {

				let comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
				$('.number > span').each(function () {
					let $this = $(this),
						num = $this.data('number');
					$this.animateNumber(
						{
							number: num,
							numberStep: comma_separator_number_step
						}, 7000
					);
				});

			}

		}, { offset: '95%' });

	}
	counter();

});